/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tubitak;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import org.apache.commons.lang.ArrayUtils;
import org.deeplearning4j.berkeley.Pair;
import org.deeplearning4j.examples.nlp.paragraphvectors.tools.LabelSeeker;
import org.deeplearning4j.examples.nlp.paragraphvectors.tools.MeansBuilder;
import org.deeplearning4j.models.embeddings.inmemory.InMemoryLookupTable;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.paragraphvectors.ParagraphVectors;
import org.deeplearning4j.models.word2vec.VocabWord;
import org.deeplearning4j.text.documentiterator.FileLabelAwareIterator;
import org.deeplearning4j.text.documentiterator.LabelAwareIterator;
import org.deeplearning4j.text.documentiterator.LabelledDocument;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.deeplearning4j.ui.standalone.ClassPathResource;
import org.nd4j.linalg.api.ndarray.INDArray;

/**
 *
 * @author ozgecan
 */
public class NewsClassification {

    public List<String> stopWordList;

    public NewsClassification() throws FileNotFoundException {
        stopWordList = new ArrayList<String>();
        stopWordList = stopWordList();
    }

    public ArrayList<String> stopWordList() throws FileNotFoundException {
        String path =System.getProperty("user.dir") + "/src/main/resources/stop-words_turkish_1_tr.txt";
        ArrayList<String> list;
        try (Scanner s = new Scanner(new File(path))) {
            list = new ArrayList<String>();
            while (s.hasNext()) {
                list.add(s.next());
            }
        }
        return list;
    }

    public void similarity() throws FileNotFoundException {
//        ClassPathResource resource = new ClassPathResource("paravec/news1");

        String path =System.getProperty("user.dir") + "/src/main/resources/paravec/news1";
        File resourcesFile = new File(path);
        // build a iterator for our dataset
        LabelAwareIterator iterator = new FileLabelAwareIterator.Builder()
                .addSourceFolder(resourcesFile)
                .build();

        TokenizerFactory t = new DefaultTokenizerFactory();
        t.setTokenPreProcessor(new CommonPreprocessor());

        // ParagraphVectors training configuration
        ParagraphVectors paragraphVectors = new ParagraphVectors.Builder()
                .learningRate(0.05)
                .minLearningRate(0.001)
                .batchSize(1000)
                .epochs(15)
                .stopWords(stopWordList)
                .iterate(iterator)
                .trainWordVectors(true)
                .tokenizerFactory(t)
                .build();
        // Start model training
        paragraphVectors.fit();

        WordVectorSerializer.writeWordVectors(paragraphVectors, path + "/modelClassifierDocument.txt");
        String path2 =System.getProperty("user.dir") + "/src/main/resources/paravec/news2";
        File resourcesFile2 = new File(path2);
        FileLabelAwareIterator unlabeledIterator = new FileLabelAwareIterator.Builder()
                .addSourceFolder(resourcesFile2)
                .build();

        
        String path3 =System.getProperty("user.dir") + "/src/main/resources/paravec/news2/dunya";
        File resourcesFile3 = new File(path3);
        File[] listOfFiles = resourcesFile3.listFiles();
        for (int i = 0; i < listOfFiles.length; i++) {
            MeansBuilder meansBuilder = new MeansBuilder((InMemoryLookupTable<VocabWord>) paragraphVectors.getLookupTable(), t);
            LabelSeeker seeker = new LabelSeeker(iterator.getLabelsSource().getLabels(), (InMemoryLookupTable<VocabWord>) paragraphVectors.getLookupTable());
            LabelledDocument document = unlabeledIterator.nextDocument();

            INDArray documentAsCentroid = meansBuilder.documentAsVector(document);
            List<Pair<String, Double>> scores = seeker.getScores(documentAsCentroid);

            System.out.println("'" + listOfFiles[i].getName() + "' dökümanı için sonuçlar aşağıdadır.");

            ArrayList<Double> doubles = new ArrayList<Double>();
            for (Pair<String, Double> score : scores) {

                doubles.add(score.getSecond());
                //  System.out.println (score.getSecond());
            }
            Double d = Collections.max(doubles);
            //  System.out.println(d);
            for (Pair<String, Double> score : scores) {
                if (Objects.equals(score.getSecond(), d)) {
                    System.out.println(listOfFiles[i].getName() + "   " + score.getFirst() + "   " + score.getSecond());
                }
            }

        }
    }

    public void simi() throws FileNotFoundException {
        TokenizerFactory t = t = new DefaultTokenizerFactory();
        t.setTokenPreProcessor(new CommonPreprocessor());

//        ClassPathResource unlabeledResource = new ClassPathResource("paravec/news2/dunya");
        ClassPathResource unlabeledResource2 = new ClassPathResource("paravec/news2");
        
        String path=System.getProperty("user.dir") + "/src/main/resources/paravec/news2/dunya";
        File file=new File(path);
        
        ParagraphVectors vector = WordVectorSerializer.readParagraphVectorsFromText(unlabeledResource2.getFile().getParent() + "/modelClassifierDocument.txt");
        FileLabelAwareIterator unlabeledIterator = new FileLabelAwareIterator.Builder()
                .addSourceFolder(file)
                .build();

      
        File[] listOfFiles = file.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            MeansBuilder meansBuilder = new MeansBuilder((InMemoryLookupTable<VocabWord>) vector.getLookupTable(), t);
            LabelSeeker seeker = new LabelSeeker(unlabeledIterator.getLabelsSource().getLabels(), (InMemoryLookupTable<VocabWord>) vector.getLookupTable());
            LabelledDocument document = unlabeledIterator.nextDocument();

            INDArray documentAsCentroid = meansBuilder.documentAsVector(document);
            List<Pair<String, Double>> scores = seeker.getScores(documentAsCentroid);

            System.out.println("'" + listOfFiles[i].getName() + "' dökümanı için sonuçlar aşağıdadır.");

            ArrayList<Double> doubles = new ArrayList<Double>();
            for (Pair<String, Double> score : scores) {

                doubles.add(score.getSecond());
                //  System.out.println (score.getSecond());
            }
            Double d = Collections.max(doubles);
            //  System.out.println(d);
            for (Pair<String, Double> score : scores) {
                if (Objects.equals(score.getSecond(), d)) {
                    System.out.println(listOfFiles[i].getName() + "   " + score.getFirst() + "   " + score.getSecond());
                }
            }
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        NewsClassification document = new NewsClassification();
        document.similarity();
//       document.simi();
    }
}
