package com.mycompany.tubitak;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.FileNotFoundException;
import java.net.SocketException;
import java.util.Collection;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.deeplearning4j.spark.models.embeddings.word2vec.Word2Vec;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;

/**
 *
 * @author ozgecan
 */
public class Word2VecSparkSimilarity {

    public static String testIP = null;

    public Word2Vec similarityWord() throws SocketException, FileNotFoundException, Exception {
        SparkConf sparkConf = new SparkConf();
        sparkConf.set("spark.driver.host", "127.0.0.1");
        sparkConf.setMaster("local[*]");
        sparkConf.setAppName("Spark shell");
        try (JavaSparkContext sc = new JavaSparkContext(sparkConf)) {
                 String path = System.getProperty("user.dir") + "/src/main/resources/text8_1";
            JavaRDD<String> corpus = sc.textFile(path);
            TokenizerFactory t = new DefaultTokenizerFactory();
            t.setTokenPreProcessor(new CommonPreprocessor());
            Word2Vec word2Vec = new Word2Vec.Builder()
                    .setNGrams(1)
                    .tokenizerFactory(t)
                    .seed(42L)
                    .negative(10)
                    .useAdaGrad(false)
                    .layerSize(50)
                    .windowSize(5)
                    .learningRate(0.05)
                    .minLearningRate(0.0001)
                    .iterations(1)
                    .batchSize(1000)
                    .minWordFrequency(5)
                    .useUnknown(true)
                    .build();
            word2Vec.train(corpus);
            sc.stop();
            return word2Vec;
        }
    }

    public void wordsNearest(Word2Vec vec, String word) throws FileNotFoundException {
        Collection<String> list = vec.wordsNearest(word, 10);
        System.out.println(word + " ye benzeyen 10 kelime  aşağıdaki gibidir");
        for(String l:list){
            System.out.println(l);
        }
    }

    public void similarWordsRate(Word2Vec vec, String word1, String word2) {
        double sim = vec.similarity(word1, word2);
        System.out.println(word1 + " / " + word2 + " = " + sim);
    }

    public static void main(String[] args) throws FileNotFoundException, Exception {
        Word2VecSparkSimilarity similarityNews = new Word2VecSparkSimilarity();
        Word2Vec vec = similarityNews.similarityWord();
        similarityNews.similarWordsRate(vec,"king","queen");
        similarityNews.wordsNearest(vec, "king");
    }
}
