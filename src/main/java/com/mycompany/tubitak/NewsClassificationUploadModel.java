/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tubitak;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.deeplearning4j.berkeley.Pair;
import org.deeplearning4j.examples.nlp.paragraphvectors.tools.LabelSeeker;
import org.deeplearning4j.examples.nlp.paragraphvectors.tools.MeansBuilder;
import org.deeplearning4j.models.embeddings.inmemory.InMemoryLookupTable;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.paragraphvectors.ParagraphVectors;
import org.deeplearning4j.models.word2vec.VocabWord;
import org.deeplearning4j.text.documentiterator.FileLabelAwareIterator;
import org.deeplearning4j.text.documentiterator.LabelledDocument;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.deeplearning4j.ui.standalone.ClassPathResource;
import org.nd4j.linalg.api.ndarray.INDArray;

/**
 *
 * @author ozgecan
 */
public class NewsClassificationUploadModel {

    public TokenizerFactory t;

    public NewsClassificationUploadModel() {
        t = new DefaultTokenizerFactory();
        t.setTokenPreProcessor(new CommonPreprocessor());
    }

    public ParagraphVectors uploadModel(String modelPath) throws FileNotFoundException {
        ParagraphVectors vector = WordVectorSerializer.readParagraphVectorsFromText(modelPath);
        return vector;
    }

    public void similarity(String sourceTestName, ParagraphVectors vector) throws FileNotFoundException {
//        ClassPathResource unlabeledResource = new ClassPathResource("paravec/news2");
        String path = System.getProperty("user.dir") + "/src/main/resources/paravec/news2";
        File resourcesFile = new File(path);
        FileLabelAwareIterator unlabeledIterator = new FileLabelAwareIterator.Builder()
                .addSourceFolder(resourcesFile)
                .build();

//        ClassPathResource unlabeledResource2 = new ClassPathResource("paravec/news2/dunya");
        String path2 = System.getProperty("user.dir") + "/src/main/resources/paravec/news2/dunya";
        File resourcesFile2 = new File(path2);
        File[] listOfFiles = resourcesFile2.listFiles();
        for (int i = 0; i < listOfFiles.length; i++) {
            MeansBuilder meansBuilder = new MeansBuilder((InMemoryLookupTable<VocabWord>) vector.getLookupTable(), t);
            LabelSeeker seeker = new LabelSeeker(vector.getLabelsSource().getLabels(), (InMemoryLookupTable<VocabWord>) vector.getLookupTable());
            LabelledDocument document = unlabeledIterator.nextDocument();

            INDArray documentAsCentroid = meansBuilder.documentAsVector(document);
            List<Pair<String, Double>> scores = seeker.getScores(documentAsCentroid);

            System.out.println("'" + listOfFiles[i].getName() + "' dökümanı için sonuçlar aşağıdadır.");

            ArrayList<Double> doubles = new ArrayList<Double>();
            for (Pair<String, Double> score : scores) {

                doubles.add(score.getSecond());
                //  System.out.println (score.getSecond());
            }
            Double d = Collections.max(doubles);

            for (Pair<String, Double> score : scores) {
                if (Objects.equals(score.getSecond(), d)) {
                    System.out.println(listOfFiles[i].getName() + "   " + score.getFirst() + "   " + score.getSecond());
                }
            }
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        NewsClassificationUploadModel uploadModel = new NewsClassificationUploadModel();
//        ClassPathResource unlabeledResource = new ClassPathResource("paravec/news2");
        String path = System.getProperty("user.dir") + "/src/main/resources/habermodel";
        ParagraphVectors paragraphVectors = uploadModel.uploadModel(path);
        uploadModel.similarity("news2/dunya", paragraphVectors);
    }
}
